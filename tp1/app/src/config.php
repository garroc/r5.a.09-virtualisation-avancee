<?php

$dsn = 'pgsql:host=' . (getenv('DB_HOST') ?? 'localhost') . ';port=' . (getenv('DB_PORT') ?? '5432') . ';dbname=' . (getenv('DB_NAME') ?? 'localdb');
$username = getenv('DB_USER') ?? 'localuser';
$password = getenv('DB_PASS') ?? 'localpassword';

try {
    $pdo = new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}